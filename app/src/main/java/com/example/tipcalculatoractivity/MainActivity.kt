package com.example.tipcalculatoractivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        calculate.setOnClickListener({
            if(!totalBeforeTip.text.isEmpty()) {
                var tip = 0.0
                val totalBefore = totalBeforeTip.text.toString()
                if (tenP.isChecked) {
                    tip = totalBefore.toDouble()
                    tip*=.1
                    outputTip.text = tip.toString()
                }
                if (twentyP.isChecked) {
                    tip = totalBefore.toDouble()
                    tip*=.2
                    outputTip.text = tip.toString()
                }
                if (thirtyP.isChecked) {
                    tip = totalBefore.toDouble()
                    tip*=.3
                    outputTip.text = tip.toString()
                }
                val temp = tip + totalBefore.toDouble()
                outputTotal.text = temp.toString()

            }
        })

    }
}
